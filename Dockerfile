FROM gradle:jdk11-alpine
COPY . /home/gradle/source
WORKDIR /home/gradle/source
RUN gradle build

FROM openjdk:11-jre-slim
COPY --from=0 /home/gradle/source/build/libs/covid_alert_back_localisation-0.0.1-SNAPSHOT.jar /app/app.jar
WORKDIR /app
EXPOSE 9093
ENTRYPOINT ["java", "-jar", "app.jar"]