package fr.poly.mtp.ig5.iwa.localisation.entity;

import java.math.BigDecimal;
import java.util.Date;

public class LocalisationDAO {

    private BigDecimal latitude;
    private BigDecimal longitude;
    private Date location_date;
    private String user;


    public BigDecimal getLatitude() {
        return latitude;
    }

    public void setLatitude(BigDecimal latitude) {
        this.latitude = latitude;
    }

    public BigDecimal getLongitude() {
        return longitude;
    }

    public void setLongitude(BigDecimal longitude) {
        this.longitude = longitude;
    }

    public Date getLocation_date() {
        return location_date;
    }

    public void setLocation_date(Date location_date) {
        this.location_date = location_date;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "Localisation{" +
                "latitude=" + latitude +
                ", longitude=" + longitude +
                ", location_date=" + location_date +
                ", user='" + user + '\'' +
                '}';
    }

    /**
     * Compare if the given localisation is close to this (<=10m)
     * @param localisation
     * @return
     */
    public boolean closeTo(Localisation localisation){
        int earthRadius = 6371; // Radius of the earth in km
        double differenceLat = deg2rad(this.latitude.subtract(localisation.getLatitude()));
        double differenceLon = deg2rad(this.longitude.subtract(localisation.getLongitude()));
        double a =
                Math.sin(differenceLat/2) * Math.sin(differenceLat/2) +
                        Math.cos(deg2rad(this.latitude)) * Math.cos(deg2rad(localisation.getLatitude())) *
                                Math.sin(differenceLon/2) * Math.sin(differenceLon/2)
                ;
        double b = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        double distance = earthRadius * b; // Distance in km
        return distance <= 0.010;
    }

    private double deg2rad(BigDecimal deg) {
        return deg.doubleValue() * (Math.PI/180);
    }

    public boolean isNotNull() {
        if (this.latitude != null && this.longitude != null && this.location_date != null && this.user != null) {
            return true;
        } else {
            return false;
        }
    }
}
