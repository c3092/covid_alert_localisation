package fr.poly.mtp.ig5.iwa.localisation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "fr")
public class ApplicationLocalisation {

    public static void main(String[] args) {
        SpringApplication.run(ApplicationLocalisation.class, args);
    }

}
