package fr.poly.mtp.ig5.iwa.localisation.config;

import fr.poly.mtp.ig5.iwa.localisation.entity.Localisation;
import fr.poly.mtp.ig5.iwa.localisation.repository.LocalisationRepository;
import fr.poly.mtp.ig5.iwa.localisation.service.LocalisationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Configuration
@EnableScheduling
public class ScheduledConfig {

    @Autowired
    private LocalisationRepository localisationRepository;


    @Scheduled(cron = "0 0 6 * * *")
    public void removeOutDatedLocalisations() {
        long currentDate = new Date().getTime();
        List<Localisation> allLocalisations = localisationRepository.findAll();
        List<Localisation> localisationsToDelete = allLocalisations.stream()
                .filter(localisation -> localisation.isOutDated(currentDate))
                .collect(Collectors.toList());
        System.out.println(localisationsToDelete.size() +" Localisations deleted");
        localisationRepository.deleteAll(localisationsToDelete);
    }
}
