package fr.poly.mtp.ig5.iwa.localisation.controller;

import fr.poly.mtp.ig5.iwa.localisation.entity.Localisation;
import fr.poly.mtp.ig5.iwa.localisation.entity.LocalisationLists;
import fr.poly.mtp.ig5.iwa.localisation.entity.UserId;
import fr.poly.mtp.ig5.iwa.localisation.service.LocalisationService;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import java.security.Principal;
import java.util.List;

@RestController
@RolesAllowed("user")
@RequestMapping("/localisation")
public class LocalisationController {

    private LocalisationService localisationService;


    public LocalisationController(LocalisationService localisationService) {
        this.localisationService = localisationService;
    }


    @GetMapping
    public List<Localisation> getAllLocalisations(){
        return localisationService.findAll();
    }


    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    Localisation newLocalisation(@RequestBody Localisation newLocalisation) {
        return localisationService.save(newLocalisation);
    }

    @PreAuthorize("#id.equals(#principal.name)")
    @GetMapping("/user/{id}")
    public LocalisationLists getLocalisationByUserId(@PathVariable("id") String id, @RequestHeader (name="Authorization") String token, Principal principal){

        return localisationService.getUserLocalisation(id);
    }

    //@PreAuthorize("#id.equals(#principal.name)")
    @GetMapping("{id}")
    public Localisation getLocalisationById(@PathVariable("id") Long id, @RequestHeader (name="Authorization") String token, Principal principal){
        return localisationService.find(id);
    }

    @DeleteMapping("{id}")
    public void delete(@PathVariable Long id){
        localisationService.delete(id);
    }

    @PutMapping("{id}")
    public Localisation update(@PathVariable Long id, @RequestBody Localisation localisation){
        return localisationService.update(id,localisation);
    }

    @PostMapping("/positive")
    public void positiveUser(@RequestBody UserId userId) {
        if (!userId.getUserId().equals("")) {
            localisationService.registerContactsSuspect(userId.getUserId());
        }
    }
}
