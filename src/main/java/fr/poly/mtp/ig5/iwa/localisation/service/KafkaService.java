package fr.poly.mtp.ig5.iwa.localisation.service;

import fr.poly.mtp.ig5.iwa.localisation.config.KafkaConfig;
import fr.poly.mtp.ig5.iwa.localisation.entity.LocalisationDAO;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.common.TopicPartition;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;


public class KafkaService {


    private final Consumer<String, LocalisationDAO> consumer = KafkaConfig.locationConsumer();

    public KafkaService(){}

    /**
     * Retrieves all the locations contained in Kafka
     */
    public ConsumerRecords<String, LocalisationDAO> kafkaGetAllMessage() {
        List<TopicPartition> partitions = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            TopicPartition topicPartition = new TopicPartition(System.getenv("KAFKA_TOPIC"), i);
            partitions.add(topicPartition);
        }
        consumer.assign(partitions);
        // We want to read all the messages
        consumer.seekToBeginning(partitions);
        return consumer.poll(Duration.ofMillis(5000));
    }





}
