package fr.poly.mtp.ig5.iwa.localisation.repository;

import fr.poly.mtp.ig5.iwa.localisation.entity.Localisation;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;

public interface LocalisationRepository  extends JpaRepository<Localisation,Long> {

    List<Localisation> findByUser(String user_id);
    List<Localisation> findByUserAndLocationDate(String user_id, Date location_date);
}
