package fr.poly.mtp.ig5.iwa.localisation.entity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;

@Access(AccessType.FIELD)
@Entity(name = "location")
public class Localisation {

    private @Id @GeneratedValue(strategy = GenerationType.IDENTITY) Long location_id;
    private BigDecimal latitude;
    private BigDecimal longitude;

    @Column(name= "location_date")
    private Date locationDate;

    @Column(name= "user_id")
    private String user;

    public Localisation(){

    }

    public Long getLocation_id() {
        return location_id;
    }

    public void setLocation_id(Long location_id) {
        this.location_id = location_id;
    }

    public BigDecimal getLatitude() {
        return latitude;
    }

    public void setLatitude(BigDecimal latitude) {
        this.latitude = latitude;
    }

    public BigDecimal getLongitude() {
        return longitude;
    }

    public void setLongitude(BigDecimal longitude) {
        this.longitude = longitude;
    }

    public Date getLocationDate() {
        return locationDate;
    }

    public void setLocationDate(Date locationDate) {
        this.locationDate = locationDate;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "Localisation{" +
                "latitude=" + latitude +
                ", longitude=" + longitude +
                ", location_date=" + locationDate +
                ", user='" + user + '\'' +
                '}';
    }

    public boolean isNotNull() {
        if (this.latitude != null && this.longitude != null && this.locationDate != null && this.user != null) {
            return true;
        } else {
            return false;
        }
    }

    public boolean isOutDated(long currentDate) {
        // If no date is sent, we use the moment the date was saved
        long localisationDate = this.getLocationDate().getTime();

        long diffInMs = Math.abs(currentDate - localisationDate);

        return diffInMs > 604800000;
    }

    public boolean equals(LocalisationDAO o) {
        return Objects.equals(locationDate, o.getLocation_date()) && Objects.equals(user, o.getUser());
    }

    @Override
    public int hashCode() {
        return Objects.hash(location_id, latitude, longitude, locationDate, user);
    }
}