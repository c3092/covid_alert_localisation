package fr.poly.mtp.ig5.iwa.localisation.exceptions;

public class NoLocalisationFoundException extends RuntimeException{

    public NoLocalisationFoundException(Long  id){
        super("No user found for this id: " + id);
    }


}
