package fr.poly.mtp.ig5.iwa.localisation.service;


import fr.poly.mtp.ig5.iwa.localisation.entity.LocalisationLists;
import fr.poly.mtp.ig5.iwa.localisation.entity.Localisation;
import fr.poly.mtp.ig5.iwa.localisation.entity.LocalisationDAO;
import fr.poly.mtp.ig5.iwa.localisation.exceptions.NoLocalisationFoundException;
import fr.poly.mtp.ig5.iwa.localisation.repository.LocalisationRepository;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class LocalisationService {

    @Autowired
    LocalisationRepository localisationRepository;

    KafkaService kafkaService = new KafkaService();

    public LocalisationService(){}

    public List<Localisation> findAll() {
        return localisationRepository.findAll();
    }

    public List<Localisation> findByUserId(String user_id) {
        return localisationRepository.findByUser(user_id);
    }

    public Localisation find(Long id) {
        return localisationRepository.findById(id).orElseThrow(() -> new NoLocalisationFoundException(id));
    }

    public Localisation save(Localisation newLocalisation) {
        //kafkaTemplate.send(TOPIC, "message");
        return localisationRepository.saveAndFlush(newLocalisation);
    }

    public void delete(Long id){
        localisationRepository.deleteById(id);
    }

    public Localisation update(@PathVariable Long id, @RequestBody Localisation localisation) {
        Localisation existingLocalisation = this.find(id);
        BeanUtils.copyProperties(localisation, existingLocalisation, "location_id");
        return this.save(existingLocalisation);
    }


    public void registerContactsSuspect(String idUser) {
        long currentDate = new Date().getTime();
        List<LocalisationDAO> contactsLocalisation = new ArrayList<>();
        List<Localisation> userLocalisations = new ArrayList<>();
        List<Localisation> suspectsLocalisations = new ArrayList<>();


        //Get all LocalisationDAO
        ConsumerRecords<String, LocalisationDAO> localisations = kafkaService.kafkaGetAllMessage();

        //Get locations only < 7 days and store in seperate array the LocalisationDAO of User positive
        for (ConsumerRecord<String, LocalisationDAO> localisationRecord : localisations) {
            LocalisationDAO localisationDAO = localisationRecord.value();

            // If no date is sent, we use the moment the date was saved
            long localisationDate = localisationDAO.getLocation_date()== null ?
                    localisationRecord.timestamp() :
                    localisationDAO.getLocation_date().getTime();
            long diffInMs = Math.abs(currentDate - localisationDate);

            if (diffInMs <= 604800000) {

                if (localisationDAO.getUser().equals(idUser)) {
                    System.out.println(localisationDAO.getUser() + " " + localisationDAO.getLocation_date());
                    List<Localisation> localisationDB = localisationRepository.findByUserAndLocationDate(localisationDAO.getUser(),localisationDAO.getLocation_date());
                    System.out.println("size : "+ localisationDB.isEmpty());
                    if (localisationDB.isEmpty()) {
                        Localisation localisation = new Localisation();
                        localisation.setLatitude(localisationDAO.getLatitude());
                        localisation.setLongitude(localisationDAO.getLongitude());
                        localisation.setLocationDate(localisationDAO.getLocation_date());
                        localisation.setUser(localisationDAO.getUser());
                        userLocalisations.add(localisation);
                    }
                } else {
                    contactsLocalisation.add(localisationDAO);
                }
            }
        }
        // Compare users localisations and user positive && store the one too close (2m)
        for (Localisation userPos: userLocalisations) {
            for (LocalisationDAO contactPosDAO: contactsLocalisation) {
                if (contactPosDAO.isNotNull() && userPos.isNotNull()) {
                    if (contactPosDAO.closeTo(userPos)) {
                        List<Localisation> localisationDB = localisationRepository.findByUserAndLocationDate(contactPosDAO.getUser(),contactPosDAO.getLocation_date());
                        if (localisationDB.isEmpty()) {
                            boolean same = false;
                            for(Localisation l: suspectsLocalisations) {
                                if(l.equals(contactPosDAO)) {
                                    same = true;
                                }
                            }
                            if (!same) {
                                System.out.println("ADDING " + contactPosDAO);
                                Localisation contactPos = new Localisation();
                                contactPos.setLatitude(contactPosDAO.getLatitude());
                                contactPos.setLongitude(contactPosDAO.getLongitude());
                                contactPos.setLocationDate(contactPosDAO.getLocation_date());
                                contactPos.setUser(contactPosDAO.getUser());
                                suspectsLocalisations.add(contactPos);
                            }
                        }

                    }
                }
            }
        }

        if (!userLocalisations.isEmpty()) {
            localisationRepository.saveAll(suspectsLocalisations);
            localisationRepository.saveAll(userLocalisations);
            localisationRepository.flush();
        }

    }

    public LocalisationLists getUserLocalisation(String id) {
        List<Localisation> userLocalisations = new ArrayList<>();
        List<Localisation> userPositiveLocalisations = localisationRepository.findByUser(id);

        ConsumerRecords<String, LocalisationDAO> localisations = kafkaService.kafkaGetAllMessage();

        for (ConsumerRecord<String, LocalisationDAO> localisationRecord : localisations) {
            LocalisationDAO localisationDAO = localisationRecord.value();
            if (localisationDAO.getUser().equals(id)) {
                Localisation localisation = new Localisation();
                localisation.setLatitude(localisationDAO.getLatitude());
                localisation.setLongitude(localisationDAO.getLongitude());
                localisation.setLocationDate(localisationDAO.getLocation_date());
                localisation.setUser(localisationDAO.getUser());
                userLocalisations.add(localisation);
            }
        }

        return new LocalisationLists(userPositiveLocalisations,userLocalisations);

    }





}
