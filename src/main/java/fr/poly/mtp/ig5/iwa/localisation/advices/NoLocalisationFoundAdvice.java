package fr.poly.mtp.ig5.iwa.localisation.advices;

import fr.poly.mtp.ig5.iwa.localisation.exceptions.NoLocalisationFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class NoLocalisationFoundAdvice {

    @ResponseBody
    @ExceptionHandler(NoLocalisationFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public String NoUserFoundAdvice(NoLocalisationFoundException ex){
        return ex.getMessage();
    }
}
