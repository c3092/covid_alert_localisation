package fr.poly.mtp.ig5.iwa.localisation.entity;

import java.util.List;

public class LocalisationLists {
    private List<Localisation> positive;
    private List<Localisation> normal;

    public LocalisationLists(List<Localisation> positive, List<Localisation> normal) {
        this.positive = positive;
        this.normal = normal;
    }

    public List<Localisation> getPositive() {
        return positive;
    }

    public List<Localisation> getNormal() {
        return normal;
    }
}
